import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:js_acc_tax/screen/home/home_screen.dart';

final GoRouter router = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/home',
      builder: (BuildContext context, GoRouterState state) {
        return const HomeScreen();
      },
    ),

  ],
);
